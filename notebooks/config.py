datamart_path = '../data/processed/datamarts.{date}.sqlite'

# TODO
cmc_endpoints = {
    # Список токенов на площадке
    'cryptocurrency/map': dict(
        path_tpl='../data/raw/coinmarketcap/{date}/cryptocurrency_map/{page_num}.json',
    ),
    # Детали по токену
    'cryptocurrency/info': dict(
        path_tpl='../data/raw/coinmarketcap/{date}/cryptocurrency_info/{coin_id}.json',
    ),
    # Последние цены на токены
    'cryptocurrency/listings/latest': dict(
        path_tpl='../data/raw/coinmarketcap/{date}/cryptocurrency_listings_latest/{page_num}.json',
    ),
    # # Список бирж на площадке
    # 'exchange/map': dict(
    #     path_tpl='../data/raw/coinmarketcap/{date}/exchange_map/{page_num}.json',
    # ),
    # # Детали по бирже
    # 'exchange/info': dict(
    #     path_tpl='../data/raw/coinmarketcap/{date}/exchange_info/{exchange}.json',
    # ),
    # # Кошельки по бирже
    # 'exchange/assets': dict(
    #     path_tpl='../data/raw/coinmarketcap/{date}/exchange_assets/{exchange}.json',
    # ),
}

lcw_endpoints = {
    # История капитализации всего крипторынка
    'overview/history': dict(
        path_tpl='../data/raw/livecoinwatch/{date}/overview_history/{currency}.json',
    ),
    # Список токенов на площадке
    'coins/list': dict(
        path_tpl='../data/raw/livecoinwatch/{date}/coins_list/{page_num}.json',
    ),
    # История котировок токенов
    'coins/single/history': dict(
        path_tpl='../data/raw/livecoinwatch/{date}/coins_single_history/{coin_code}.json',
    ),
}