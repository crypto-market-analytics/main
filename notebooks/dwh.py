import os

from typing import List

import sqlalchemy
import pandas as pd


def ensure_dir(file_path):
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
    return file_path


def unnest_dict(df, column, keys, drop_source_column=False):
    """
    Given a dataframe and its column containing dicts,
    fetch given keys from it to separate columns named
    `column_key`.
    """
    for key in keys:
        df[f"{column}_{key}"] = df[column].apply(
            lambda dct: dct.get(key, None) if isinstance(dct, dict) else None
        )
    if drop_source_column:
        del df[column]


def convert_datetimes(df, columns: List[str], format: str = None):
    """
    Given a dataframe and list of columns containing strings with dates,
    converts those columns into real datetimes.

    Arguments:
    ----------
    df: a dataframe
    columns: list of columns to be converted
    format: optional, a format to easen the parsing for pandas
    """
    for col in columns:
        df[col] = pd.to_datetime(df[col], format=format)


def date_trunc(series: pd.Series, period: str, to_date: bool = None):
    """
    Truncate datetime to given period.

    Try to guess by the period if we should truncate to date
    or keep time info.

    Arguments:
    ----------
    series: 
        a series of datetimes
    period: 
        a period, see `pandas.to_period`
    to_date: default = None
        True — truncate the resulting timestamp to dates
        False — keep time info
        None — let it guess
    """
    if to_date is None:
        to_date = False
        if period in ('Y', 'm', 'w'):
            to_date = True
    series = series.dt.to_period(period).dt.to_timestamp()
    if to_date:
        series = series.dt.date
    return series


def reverse_cumsum(df, axis=0):
    """
    Given a table, calculate its cumulative sum
    from the last row/col to the first one.
    
    Useful for calculating retention plots.
    """
    if axis == 0:
        df = df.loc[df.index[::-1]]
        df = df.cumsum(axis=0)
        df = df.loc[df.index[::-1]]
    elif axis == 1:
        df = df[df.columns[::-1]]
        df = df.cumsum(axis=1)
        df = df[df.columns[::-1]]        
    else:
        raise NotImplementedError("Don't know about axis not 0 or 1!")
    return df


def handle_duplicates(df, key, fmt=None, do=None):
    """
    Check a dataframe for duplicates on given key(s).
    Optionally, drop those duplicates.

    Arguments:
    ----------
    df: a dataframe
    key: column (string) or columns (list of stings) to check
    fmt: format string to print logs, gets row columns as kwargs
    do: None or 'drop' (works inplace!)
    """
    duplicates = df[df[key].duplicated()]
    print("Found %i duplicates on %s" % (len(duplicates), str(key)))
    if fmt:
        print(', '.join(duplicates.apply(lambda row: fmt.format(**row), axis=1)))
    if do == 'drop':
        df.drop_duplicates([key], inplace=True)
        print(f"Dropped them (kept exactly 1 record on each {key} value).")


class DatamartKeeper:
    """
    Keeps the tables in SQL storage.
    """
    def __init__(self, path):
        """
        path: a path to database files
        """
        self._path = path
        self._datamarts = {}

    @property
    def m(self):
        return self._datamarts

    @property
    def datamart_names(self):
        return [*self._datamarts.keys()]

    
    def __repr__(self):
        return f"""\
Datamart keeper
===============
datamarts: {', '.join(self.datamart_names)}
"""
              
    def _check_data(self):
        pass
        
    @property
    def dbconn(self):
        if not hasattr(self, '_dbconn'):
            path = os.path.abspath(self._path)
            self._dbconn = sqlalchemy.create_engine(
                f'sqlite:///{path}'
            )
        return self._dbconn
    
    def dump_to_db(self):
        for mart_name in self.datamart_names:
            self.m[mart_name].to_sql(
                mart_name,
                con=self.dbconn, 
                if_exists='replace', 
                index=False
            )
            
    def sql(self, query):
        return pd.read_sql(query, con=self.dbconn)